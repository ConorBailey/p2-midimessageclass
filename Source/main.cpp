//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "MidiMessage.hpp"

int main (int argc, const char* argv[])
{

    // insert code here...
    MidiMessage note;
    int noteNum;
    int channelNum;
    float velocity;
    
    std::cout << "insert your note number\n";
    std::cin >> noteNum;
    note.setNoteNumber(noteNum);
    
    std::cout << "\ninsert your velocity\n";
    std::cin >> velocity;
    note.setVelocity(velocity);
    
    std::cout << "\ninsert your channel number\n";
    std::cin >> channelNum;
    note.setChannelNumber(channelNum);
    
    std::cout << "note value = " << note.getNoteNumber() <<"\nfrequency = " << note.getMidiNoteInHertz() <<"\namp = " << note.getFloatVelocity() << "\nchannel number = " << note.getChannelNumber() << "\n";
    return 0;
}

